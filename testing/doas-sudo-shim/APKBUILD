# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=doas-sudo-shim
# Note: This should be always lower than pkgver of the real sudo!
pkgver=0.1.0
pkgrel=0
pkgdesc="A shim for the sudo command that utilizes doas"
url="https://github.com/jirutka/doas-sudo-shim"
arch="noarch"
license="ISC"
depends="cmd:doas"
makedepends="asciidoctor"
subpackages="$pkgname-doc"
source="https://github.com/jirutka/doas-sudo-shim/archive/v$pkgver/$pkgname-$pkgver.tar.gz
	help-alpine.patch
	"
options="!check"  # no tests provided

build() {
	make man
}

package() {
	depends="$depends !sudo"  # this must not be defined on top-level

	make install DESTDIR="$pkgdir" PREFIX=/usr
}

sha512sums="
94335071141775715f99dbf1149471caec7918683b394c998470bb9a82fe05c90ece83ca52d548fbab33f1a3c5114627947e87a566d745fb2c0eb032a359895e  doas-sudo-shim-0.1.0.tar.gz
4e3e4fe1a72ed119c650136cc929786a9c84551c88c27f2f53b59eb638b28261d88548e33b1004f0f69a755d81c87fd3395937d7b2a07cc5efae57fdd0f4ca6c  help-alpine.patch
"
